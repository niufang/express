var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var config = require('./config/app.json')
var app = express();


app.set('views', path.join(__dirname, '/views'));
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');


app.use(bodyParser.json());


app.use(express.static(__dirname + '/src'));


var port = config['port'] || 4000;


routes(app);
app.listen(port);

console.log('------0.0------');


module.exports = app;
